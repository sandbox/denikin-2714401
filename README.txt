Purpose
-------
This simple module turns your website into a email gateway. This might be
useful if you have some other webservices/applications but wants all
messages sent from Drupal site. Another usecase could be if you have some
important mail hooks implemented and wants them to be applied for email
messages from other applications.

After module installation allowed clients would be able to send json-ed
email messages via POST request to the dedicated address - /mailspool/in .
Then Drupal perform usual routing for a received message.


Configuring on Drupal side
--------------------------
Enable module as usual, configure it at the administration page
admin/config/services/mailspool .

Choose protection key, and optionally specify IP addresses to limit access
to specific email senders.


Configuring on client side
--------------------------
Having key and being added to the list of senders, format your messages
accordingly and do the POST request. Sample pseudocode in PHP, as a
replacement for regular mail() call:

function spool_mail($to, $subject, $message, $headers = null, $parameters = null) {
	if (($curl = curl_init()) === false) {
		return false;
	}
	$data = new stdClass();
	$data->to = $to;
	$data->subject = $subject;
	$data->message = $message;
	$data->headers = $headers;
	$data->parameters = $parameters;
	curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 0,
			CURLOPT_URL => 'http://example.com/mailspool/in',
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => array(
					'key' => 'the_key_is_here',
					'data' => json_encode($data),
			)
	));
	$response = curl_exec($curl);
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);
    if (($response === false) || ($httpcode != 200)) {
		return false;
	}

	return true;
}


Credits
-------
Developed by Dennis Povshedny, https://www.drupal.org/user/117896
