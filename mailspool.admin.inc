<?php
/**
 * @file
 * Administration page for the module.
 *
 */

/**
 * Administration settings/operation form.
 */
function mailspool_admin_settings($form, &$form_state) {
  $form['mailspool_key'] = array(
    '#title' => t('Secret key'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('mailspool_key', drupal_random_key(16)),
    '#description' => t('Secret code used for authorization.'),
  );

  $ips = json_decode(variable_get('mailspool_ips', '["127.0.0.1"]'));
  $form['mailspool_ips'] = array(
    '#title' => t('Allowed IPs'),
    '#type' => 'textarea',
    '#default_value' => is_array($ips) ? implode("\r", $ips) : $ips,
    '#description' => t('Keep it blank to allow receiving mail from any addresses. Otherwise - one whitelisted IP per line.'),
  );

  $form['#submit'][] = 'mailspool_admin_settings_submit';

  return system_settings_form($form);
}

/**
 * Submit handler for settings form.
 */
function mailspool_admin_settings_submit($form, &$form_state) {
  $values = &$form_state['values'];

  $matches = array();
  $count = preg_match_all('/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/', $values['mailspool_ips'], $matches);

  if (!$count) {
    $values['mailspool_ips'] = "";
    return;
  }

  $values['mailspool_ips'] = json_encode($matches[0]);
  drupal_set_message(t('Access allowed only from %count IP addresses.', array('%count' => $count)));
}
